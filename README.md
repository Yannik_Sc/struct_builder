# struct_builder

This application generates structs, hydrators and extractors from XML definitions.

## How to use

The XML files need to be placed at a specific location.
The structs need to be in the "structs" directory and the type definitions have to be in the "types" directory.

The files in the "structs" directory are defining your structs. The files are looking like the following:
```XML
<?xml version="1.0" encoding="UTF-8" ?>
<structs>
    <struct name="my_struct">
        <field type="integer" name="id" default=""/>
        <field type="string" name="name" default=""/>
    </struct>
</structs>
```

However, in order to add fields, you need to define types for those fields.
The type entry takes
- a name, which will be used in the struct
- a cxxType, which will is the type, that is set in the C++ code
- a conversionFunction, which will be called with the Json::Value as an argument, which will transfers the Json::Value, to the C++ type. The Conversion function, can be any function, that accepts a Json::Value and outputs a C++ value.
- headers, that include the C++ type (e.g. the "header" include for the std::string type). Headers are passed in as child elements in the type. You can add as many headers as you like.

The resulting type file will look like the following one:
```XML
<?xml version="1.0" encoding="UTF-8" ?>
<types>
    <type name="integer" cxxType="int" conversionFunction="struct_converter_functions::convert_integer"/>
    <type name="string" cxxType="std::string" conversionFunction="struct_converter_functions::convert_string">
        <header file="string"/>
    </type>
</types>
```

Now, in order to generate the structs and converting functions, you have to execute the "struct_builder" application. Give it the path, where the "structs" and "types" folders are located in and it will do the trick.

For more examples, how to use the generated structs and converters, take a look into the `example` folder.

## Changelog


### 1.1.2 "Crash-fix"-update
- Fix segfault when calling help without providing files as arguments

### 1.1.1 "Options"-update
- Add console option parser
- Add option to show help
- Add option to set include guard value
- Add option to set generated files name

### 1.1.0 "Multi file"-update
- Add option to pass multiple source directories in
- Add option for output directory (files will be created there)
- Separated header and source file(s)

### 1.0.0 *Release*
- Add basic functionality
  - Add option to generate structs by XML definition
  - Add option to add native types and register custom types
  - Output will be send to stdout

