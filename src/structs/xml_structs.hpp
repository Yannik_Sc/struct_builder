#ifndef STRUCT_BUILDER_XML_STRUCTS_HPP
#define STRUCT_BUILDER_XML_STRUCTS_HPP

#include <vector>
#include <string>

namespace structs
{
    /* ** Struct definitions ** */

    struct type_field
    {
        std::string name;

        std::string type;

        bool readonly = false;

        std::string defaultValue = "";

        bool list = false;
    };

    using type_fields = std::map<std::string, struct type_field>;

    struct type_require
    {
        std::string structName;
    };

    using type_requires = std::vector<struct type_require>;

    struct type_struct
    {
        std::string name;

        type_fields fields = {};

        type_requires requirements = {};
    };

    using type_structs = std::map<std::string, struct type_struct>;

    /* ** Type definitions ** */

    struct type_header
    {
        std::string file;
    };

    using type_headers = std::vector<struct type_header>;

    struct type_type
    {
        std::string name;

        std::string cxxType;

        std::string conversionFunction;

        type_headers header = {};
    };

    using type_types = std::map<std::string, struct type_type>;
}

#endif //STRUCT_BUILDER_XML_STRUCTS_HPP
