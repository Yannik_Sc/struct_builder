#include "xml_parser.hpp"

#include <iostream>
#include <utility>

#define NODE_STRUCT_ROOT_NAME "structs"
#define NODE_STRUCT_CHILD_NAME "struct"
#define NODE_STRUCT_FIELD_NAME "field"
#define NODE_STRUCT_REQUIRE_NAME "require"

#define NODE_TYPE_ROOT_NAME "types"
#define NODE_TYPE_CHILD_NAME "type"
#define NODE_TYPE_HEADER_NAME "header"

namespace parser
{
    xml_parser::xml_parser(std::string directoryName) : directoryNames({std::move(directoryName)})
    {}

    xml_parser::xml_parser(const std::vector<std::string> &directoryNames) : directoryNames(directoryNames)
    {}

    structs::type_types xml_parser::get_types()
    {
        if (!this->initialized) {
            this->parse();

            this->initialized = true;
        }

        return this->types;
    }

    structs::type_structs xml_parser::get_structs()
    {
        if (!this->initialized) {
            this->parse();

            this->initialized = true;
        }

        return this->structs;
    }

    void xml_parser::parse()
    {
        for (auto dir : this->directoryNames) {
            this->parse_dir(dir);
        }
    }

    void xml_parser::parse_dir(const std::string &dirName)
    {
        DIR *dir = opendir(dirName.c_str());

        if (dir == nullptr) this->parse_file(dirName);

        struct dirent *ent;

        while ((ent = readdir(dir)) != nullptr) {
            std::string fullPath = dirName + "/" + ent->d_name;

            if (ent->d_type == DT_DIR && strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                this->parse_dir(fullPath);

                continue;
            }

            if (ent->d_type != DT_REG) continue;

            if (fullPath.find(".xml") == fullPath.size() - 4) {
                this->parse_file(fullPath);
            }
        }
    }

    void xml_parser::parse_file(const std::string &fileName)
    {
        xmlpp::DomParser parser;
        parser.parse_file(fileName);

        if (parser.get_document()->get_root_node()->get_name() == NODE_STRUCT_ROOT_NAME) {
            for (auto node : parser.get_document()->get_root_node()->get_children(NODE_STRUCT_CHILD_NAME))
                this->parse_struct(node);

            return;
        }

        if (parser.get_document()->get_root_node()->get_name() == NODE_TYPE_ROOT_NAME) {
            for (auto node : parser.get_document()->get_root_node()->get_children(NODE_TYPE_CHILD_NAME))
                this->parse_type(node);

            return;
        }
    }

    void xml_parser::parse_struct(xmlpp::Node *node)
    {
        auto structElement = dynamic_cast<xmlpp::Element *>(node);
        if (structElement == nullptr) return;

        std::string name = structElement->get_attribute_value("name");

        if (this->structs.find(name) == this->structs.end()) {
            structs::type_struct str{
                .name = name
            };

            this->structs.insert({name, str});
        }

        auto s_struct = &this->structs.find(name)->second;

        for (auto nodeField : node->get_children(NODE_STRUCT_FIELD_NAME)) {
            auto field = dynamic_cast<xmlpp::Element *>(nodeField);

            if (field == nullptr) continue;

            structs::type_field s_field{
                .name = field->get_attribute_value("name"),
                .type = field->get_attribute_value("type"),
                .readonly = field->get_attribute_value("readonly") == "true",
                .defaultValue = field->get_attribute_value("default"),
                .list = field->get_attribute("list") != nullptr
            };

            s_struct->fields.insert({s_field.name, s_field});
        }

        for (auto nodeField : node->get_children(NODE_STRUCT_REQUIRE_NAME)) {
            auto require = dynamic_cast<xmlpp::Element *>(nodeField);

            if (require == nullptr) continue;

            structs::type_require s_require{
                .structName = require->get_attribute_value("struct")
            };

            s_struct->requirements.push_back(s_require);
        }
    }

    void xml_parser::parse_type(xmlpp::Node *node)
    {
        auto typeElement = dynamic_cast<xmlpp::Element *>(node);
        if (typeElement == nullptr) return;

        structs::type_type type{
            .name = typeElement->get_attribute_value("name"),
            .cxxType = typeElement->get_attribute_value("cxxType"),
            .conversionFunction = typeElement->get_attribute_value("conversionFunction")
        };

        for (auto nodeHeader : node->get_children(NODE_TYPE_HEADER_NAME)) {
            auto header = dynamic_cast<xmlpp::Element *>(nodeHeader);

            if (header == nullptr) continue;

            structs::type_header headerInfo{
                .file = header->get_attribute_value("file")
            };

            type.header.push_back(headerInfo);
        }

        this->types.insert({type.name, type});
    }
}
