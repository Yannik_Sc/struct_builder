#ifndef STRUCT_BUILDER_XML_PARSER_HPP
#define STRUCT_BUILDER_XML_PARSER_HPP

#include <string>
#include <libxml++/libxml++.h>
#include "../structs/xml_structs.hpp"

namespace parser
{
    class xml_parser
    {
    protected:
        std::vector<std::string> directoryNames;

        structs::type_types types;

        structs::type_structs structs;

        bool initialized = false;

    public:
        explicit xml_parser(std::string directoryName);

        explicit xml_parser(const std::vector<std::string> &directoryNames);

        structs::type_types get_types();

        structs::type_structs get_structs();

        void parse();

    protected:
        void parse_dir(const std::string &dirName);

        void parse_file(const std::string &fileName);

        void parse_struct(xmlpp::Node *node);

        void parse_type(xmlpp::Node *node);
    };
}

#endif //STRUCT_BUILDER_XML_PARSER_HPP
