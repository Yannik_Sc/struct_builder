#include <iostream>
#include <vector>
#include <map>
#include "parser/xml_parser.hpp"
#include "builder/struct_builder.hpp"

int main(int argc, char **argv)
{
    std::vector<std::string> arguments;
    std::map<std::string, std::string> options;

    int lastIndex = 0;

    for (int i = 1; i < argc; i++) {
        std::string arg(argv[i]);

        if (arg.find("--") == 0) {
            int eqSign = arg.find('=');
            std::string value;
            std::string name = arg.substr(2, eqSign - 2);

            if (eqSign != -1) {
                value = arg.substr(eqSign + 1);
            }

            options.insert({name, value});

            continue;
        }

        arguments.emplace_back(argv[i]);
    }

    if (options.find("help") != options.end()) {
        std::cout << "Usage: " << argv[0] << " <PATH_TO_RESOURCES> <OUTPUT_PATH>" << std::endl
                  << "  Options:" << std::endl
                  << "    --help                  Prints this help" << std::endl
                  << "    --out-file-name         Sets the name for the generated files" << std::endl
                  << "    --include-guard         Sets the value for the include guard" << std::endl;

        return 0;
    }

    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <PATH_TO_RESOURCES> <OUTPUT_PATH>" << std::endl;

        return 1;
    }

    std::string outputDir = arguments[arguments.size() - 1];
    arguments.pop_back();
    std::string includeGuard = "__STRUCT_BUILDER__STRUCTS_HPP";

    if (options.find("include-guard") != options.end())
        includeGuard = options.find("include-guard")->second;

    std::string outFileName = "structs";

    if (options.find("out-file-name") != options.end())
        outFileName = options.find("out-file-name")->second;

    auto *parser = new parser::xml_parser(arguments);
    builder::struct_builder builder(parser, outputDir, includeGuard, outFileName);
    builder.build_structs();

    return 0;
}
