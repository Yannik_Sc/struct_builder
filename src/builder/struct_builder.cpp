#include <iostream>
#include <json/value.h>
#include "struct_builder.hpp"
#include "builder_exception.hpp"
#include <algorithm>

namespace builder
{

    struct_builder::struct_builder(
        parser::xml_parser *parser,
        std::string outputPath,
        std::string includeGuard,
        std::string outFileName
    )
        : parser(parser), includeGuard(includeGuard), outFileName(outFileName)
    {
        this->sourceFile = std::ofstream(outputPath + "/" + this->outFileName + ".cpp");
        this->headerFile = std::ofstream(outputPath + "/" + this->outFileName + ".hpp");

        this->sourceFile << "#include \"" + this->outFileName + ".hpp\"\n";
    }

    void struct_builder::build_structs()
    {
        this->types = this->parser->get_types();
        this->structs = this->parser->get_structs();

        this->headerFile << "#ifndef " << this->includeGuard << std::endl;
        this->headerFile << "#define " << this->includeGuard << std::endl;

        this->create_header();
        this->create_structs();

        this->headerFile << "#endif" << std::endl;
    }

    void struct_builder::create_header()
    {
        std::vector<std::string> headers{
            "converter/default_type_converter.hpp",
            "json/value.h"
        };

        for (const auto &type : types) {
            for (const auto &header : type.second.header) {
                if (std::find(headers.begin(), headers.end(), header.file) == headers.end())
                    headers.push_back(header.file);
            }
        }

        std::stringstream fileContent;

        for (const auto &file : headers) {
            fileContent << "#include \"" << file << "\"\n";
        }

        this->headerFile << fileContent.str() << std::endl;
    }

    void struct_builder::create_structs()
    {
        std::stringstream fileContent;

        std::vector<std::string> names;

        for (const auto &strct : this->structs) {
            if (std::find(names.begin(), names.end(), strct.first) == names.end()) names.push_back(strct.first);
        }

        for (const auto &name : names) {
            this->create_struct(name, fileContent);
        }

        this->headerFile << fileContent.str() << std::endl;
    }

    void struct_builder::create_struct(const std::string &name, std::stringstream &fileContent)
    {
        if (this->structs.find(name) == this->structs.end()) return;

        auto strct = this->structs.find(name)->second;

        structs::type_type structType{
            .name = strct.name,
            .cxxType = "struct " + strct.name,
            .conversionFunction = strct.name + "::hydrate_struct",
        };

        this->types.insert({strct.name, structType});

        std::stringstream localStruct;
        this->add_struct_head(localStruct, name);
        this->add_requirements(localStruct, fileContent, strct.requirements);
        this->add_struct_fields(localStruct, fileContent, strct.fields);
        this->add_struct_end(localStruct);

        fileContent << localStruct.str();

        fileContent << "namespace struct_builder::struct_converter::" << name << " {\n";
        this->sourceFile << "namespace struct_builder::struct_converter::" << name << " {\n";
        this->create_single_converter(strct, fileContent);
        this->sourceFile << "}\n";
        fileContent << "}\n";

        this->structs.erase(this->structs.find(name));
    }

    void struct_builder::add_struct_head(std::stringstream &fileContent, const std::string &structName)
    {
        fileContent << "namespace struct_builder::structs {\n";
        fileContent << "struct " << structName << " {\n";
    }

    void struct_builder::add_requirements(
        std::stringstream &fileContent,
        std::stringstream &rootFileContent,
        const structs::type_requires &requires
    )
    {
        for (const auto &require : requires) {
            auto type = this->resolve_type_name(require.structName, rootFileContent);
        }
    }

    void struct_builder::add_struct_fields(
        std::stringstream &fileContent,
        std::stringstream &rootFileContent,
        const structs::type_fields &fields
    )
    {
        for (const auto &field : fields) {
            auto type = this->resolve_type_name(field.second.type, rootFileContent);

            if (field.second.readonly) fileContent << "const ";

            fileContent << type << " " << field.second.name;

            if (!field.second.defaultValue.empty()) fileContent << " = " << this->get_default_value(field.second);

            fileContent << ";\n";
        }
    }

    void struct_builder::add_struct_end(std::stringstream &fileContent)
    {
        fileContent << "};\n";
        fileContent << "}\n";
    }

    std::string struct_builder::resolve_type_name(const std::string &typeName, std::stringstream &fileContent)
    {
        auto type = this->types.find(typeName);

        if (type == this->types.end()) {
            this->create_struct(typeName, fileContent);

            type = this->types.find(typeName);
        }

        if (type == this->types.end()) {
            std::cerr << "Type " << typeName << " is not defined" << std::endl;

            throw builder_exception(
                (char *) std::string("Type " + typeName + " is not defined (are they in correct order?)").c_str()
            );
        }

        return type->second.cxxType;
    }

    std::string struct_builder::get_default_value(const structs::type_field &field)
    {
        if (field.type == "string") { // TODO: Find better solution
            return "R\"(" + field.defaultValue + ")\"";
        }

        return field.defaultValue;
    }

    std::string struct_builder::get_extractor_function(const structs::type_field &field)
    {
        for (const auto &type : this->types) {
            if (type.second.name != field.type) continue;

            if (type.second.cxxType.find("struct ") == 0) return field.type + "::extract_struct";
        }

        return "";
    }

    void struct_builder::create_extractor(
        const structs::type_struct &typeStruct,
        std::stringstream &fileContent
    )
    {
        fileContent << "Json::Value extract_struct(const struct struct_builder::structs::" << typeStruct.name
                    << "& myStruct);\n";

        this->sourceFile << "Json::Value extract_struct(const struct struct_builder::structs::" << typeStruct.name
                         << "& myStruct){\n"
                         << "Json::Value val;";

        for (const auto &field : typeStruct.fields) {
            this->sourceFile << "val[\"" << field.second.name << "\"] = "
                             << this->get_extractor_function(field.second)
                             << "(myStruct." << field.second.name << ");\n";
        }

        this->sourceFile << "return val;\n};\n";
    }

    std::string struct_builder::get_converter_function(const structs::type_field &field)
    {
        for (const auto &type : types) {
            if (type.second.name == field.type)
                return type.second.conversionFunction;
        }

        return "";
    }

    void struct_builder::create_hydrator(
        const structs::type_struct &typeStruct,
        std::stringstream &fileContent
    )
    {
        fileContent << "struct struct_builder::structs::" << typeStruct.name
                    << " hydrate_struct(const Json::Value& value);\n";

        this->sourceFile << "struct struct_builder::structs::" << typeStruct.name
                         << " hydrate_struct(const Json::Value& value) {\n"
                         << "struct struct_builder::structs::" << typeStruct.name << " myStruct {\n";

        for (const auto &field : typeStruct.fields) {
            this->sourceFile << "." << field.second.name << " = "
                             << this->get_converter_function(field.second)
                             << "(value[\"" << field.second.name << "\"]),\n";
        }

        this->sourceFile << "};\n"
                         << "return myStruct;\n};\n";
    }

    void struct_builder::create_single_converter(
        const structs::type_struct &typeStruct,
        std::stringstream &fileContent
    )
    {
        this->create_hydrator(typeStruct, fileContent);
        this->create_extractor(typeStruct, fileContent);
    }
}
