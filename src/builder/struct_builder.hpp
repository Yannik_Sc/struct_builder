#ifndef STRUCT_BUILDER_STRUCT_BUILDER_HPP
#define STRUCT_BUILDER_STRUCT_BUILDER_HPP

#include <fstream>
#include "../parser/xml_parser.hpp"

namespace builder
{
    class struct_builder
    {
    protected:
        parser::xml_parser *parser;

        std::string includeGuard;

        std::string outFileName;

        structs::type_types types;

        structs::type_structs structs;

        std::ofstream sourceFile;

        std::ofstream headerFile;

    public:
        struct_builder(
            parser::xml_parser *parser,
            std::string outputPath,
            std::string includeGuard = "__STRUCT_BUILDER__STRUCTS_HPP",
            std::string outFileName = "structs"
        );

        void build_structs();

    protected:
        void create_header();

        void create_structs();

        void create_struct(const std::string &name, std::stringstream &fileContent);

        void add_struct_head(std::stringstream &fileContent, const std::string &structName);

        void add_requirements(
            std::stringstream &fileContent,
            std::stringstream &rootFileContent,
            const structs::type_requires &requires
        );

        void add_struct_fields(
            std::stringstream &fileContent,
            std::stringstream &rootFileContent,
            const structs::type_fields &fields
        );

        std::string resolve_type_name(const std::string &typeName, std::stringstream &fileContent);

        std::string get_default_value(const structs::type_field &field);

        void add_struct_end(std::stringstream &fileContent);

        std::string get_extractor_function(const structs::type_field &field);

        void create_extractor(
            const structs::type_struct &typeStruct,
            std::stringstream &fileContent
        );

        std::string get_converter_function(const structs::type_field &field);

        void create_hydrator(
            const structs::type_struct &typeStruct,
            std::stringstream &fileContent
        );

        void create_single_converter(
            const structs::type_struct &typeStruct,
            std::stringstream &fileContent
        );
    };
}

#endif //STRUCT_BUILDER_STRUCT_BUILDER_HPP
