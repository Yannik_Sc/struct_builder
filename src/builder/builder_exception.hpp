#ifndef STRUCT_BUILDER_BUILDER_EXCEPTION_HPP
#define STRUCT_BUILDER_BUILDER_EXCEPTION_HPP

#include <exception>

namespace builder
{
    class builder_exception : public std::exception
    {
    protected:
        char *message;
    public:
        explicit builder_exception(char *message) : message(message)
        {}

        char *get_message()
        {
            return this->message;
        }
    };
}

#endif //STRUCT_BUILDER_BUILDER_EXCEPTION_HPP
