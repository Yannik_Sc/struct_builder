#include <iostream>
#include <json/writer.h>
#include <json/reader.h>

// The import for the generated structs
#include "generated/structs.hpp"

void hydrate()
{
    Json::Value inval;

    std::cin >> inval;

    struct struct_builder::structs::example str = struct_builder::struct_converter::example::hydrate_struct(inval);
    //     ^^^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^  ^^^^^^^^^^^^^^
    //      |                        |             |                                 |        The conversion function
    //      |                        |             |                                 | The struct name (again)
    //      |                        |             The namespace, where the converter functions are located at
    //      |                        This is the name of your struct
    //      This is the namespace, where all the structs are contained in

    std::cout << "struct.full_name.firstname   = " << str.full_name.firstname << std::endl;
    std::cout << "struct.full_name.lastname    = " << str.full_name.lastname << std::endl;
    std::cout << "struct.id                    = " << str.id << std::endl;
    std::cout << "struct.my_boolean            = " << str.my_boolean << std::endl;
    std::cout << "struct.my_float              = " << str.my_float << std::endl;
    std::cout << "struct.my_int                = " << str.my_int << std::endl;
    std::cout << "struct.my_string             = " << str.my_string << std::endl;
};

void extract()
{
    struct_builder::structs::example stru{
        .full_name = {
            .firstname = "Yannik",
            .lastname = "Sc"
        }
    };

    Json::StreamWriter *wrtr = (new Json::StreamWriterBuilder())->newStreamWriter();
    Json::Value val = struct_builder::struct_converter::example::extract_struct(stru);
    //                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^  ^^^^^^^^^^^^^^
    //                 |                                 |        The conversion function
    //                 |                                 The struct name
    //                 The namespace, where the converter functions are located at

    wrtr->write(val, &std::cout);
}

int main()
{
    hydrate();
    extract();
}
