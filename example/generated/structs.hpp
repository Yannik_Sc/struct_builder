#ifndef __STRUCT_BUILDER__STRUCTS_HPP
#define __STRUCT_BUILDER__STRUCTS_HPP
#include "converter/default_type_converter.hpp"
#include "json/value.h"
#include "string"
#include "vector"

namespace struct_builder::structs {
struct other_struct {
std::string firstname;
std::string lastname;
};
}
namespace struct_builder::struct_converter::other_struct {
struct struct_builder::structs::other_struct hydrate_struct(const Json::Value& value);
Json::Value extract_struct(const struct struct_builder::structs::other_struct& myStruct);
}
namespace struct_builder::structs {
struct example {
struct other_struct full_name;
const int id = 1;
bool my_boolean = false;
float my_float = 0.0f;
int my_int = 0;
std::string my_string = R"(*empty*)";
const std::string pre_id = R"(1337_hex_hex_ID)";
};
}
namespace struct_builder::struct_converter::example {
struct struct_builder::structs::example hydrate_struct(const Json::Value& value);
Json::Value extract_struct(const struct struct_builder::structs::example& myStruct);
}
namespace struct_builder::structs {
struct example_ {
std::string firstname;
struct other_struct full_name;
std::string lastname;
};
}
namespace struct_builder::struct_converter::example_ {
struct struct_builder::structs::example_ hydrate_struct(const Json::Value& value);
Json::Value extract_struct(const struct struct_builder::structs::example_& myStruct);
}
namespace struct_builder::structs {
struct scnd_example {
const std::string name = R"(I'm a default struct :3)";
};
}
namespace struct_builder::struct_converter::scnd_example {
struct struct_builder::structs::scnd_example hydrate_struct(const Json::Value& value);
Json::Value extract_struct(const struct struct_builder::structs::scnd_example& myStruct);
}

#endif
