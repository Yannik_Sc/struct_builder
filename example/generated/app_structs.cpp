#include "app_structs.hpp"
namespace struct_builder::struct_converter::other_struct {
struct struct_builder::structs::other_struct hydrate_struct(const Json::Value& value) {
struct struct_builder::structs::other_struct myStruct {
.firstname = struct_converter_functions::convert_string(value["firstname"]),
.lastname = struct_converter_functions::convert_string(value["lastname"]),
};
return myStruct;
};
Json::Value extract_struct(const struct struct_builder::structs::other_struct& myStruct){
Json::Value val;val["firstname"] = (myStruct.firstname);
val["lastname"] = (myStruct.lastname);
return val;
};
}
namespace struct_builder::struct_converter::example {
struct struct_builder::structs::example hydrate_struct(const Json::Value& value) {
struct struct_builder::structs::example myStruct {
.full_name = other_struct::hydrate_struct(value["full_name"]),
.id = struct_converter_functions::convert_integer(value["id"]),
.my_boolean = struct_converter_functions::convert_boolean(value["my_boolean"]),
.my_float = struct_converter_functions::convert_float(value["my_float"]),
.my_int = struct_converter_functions::convert_integer(value["my_int"]),
.my_string = struct_converter_functions::convert_string(value["my_string"]),
.pre_id = struct_converter_functions::convert_string(value["pre_id"]),
};
return myStruct;
};
Json::Value extract_struct(const struct struct_builder::structs::example& myStruct){
Json::Value val;val["full_name"] = other_struct::extract_struct(myStruct.full_name);
val["id"] = (myStruct.id);
val["my_boolean"] = (myStruct.my_boolean);
val["my_float"] = (myStruct.my_float);
val["my_int"] = (myStruct.my_int);
val["my_string"] = (myStruct.my_string);
val["pre_id"] = (myStruct.pre_id);
return val;
};
}
namespace struct_builder::struct_converter::example_ {
struct struct_builder::structs::example_ hydrate_struct(const Json::Value& value) {
struct struct_builder::structs::example_ myStruct {
.firstname = struct_converter_functions::convert_string(value["firstname"]),
.full_name = other_struct::hydrate_struct(value["full_name"]),
.lastname = struct_converter_functions::convert_string(value["lastname"]),
};
return myStruct;
};
Json::Value extract_struct(const struct struct_builder::structs::example_& myStruct){
Json::Value val;val["firstname"] = (myStruct.firstname);
val["full_name"] = other_struct::extract_struct(myStruct.full_name);
val["lastname"] = (myStruct.lastname);
return val;
};
}
namespace struct_builder::struct_converter::scnd_example {
struct struct_builder::structs::scnd_example hydrate_struct(const Json::Value& value) {
struct struct_builder::structs::scnd_example myStruct {
.name = struct_converter_functions::convert_string(value["name"]),
};
return myStruct;
};
Json::Value extract_struct(const struct struct_builder::structs::scnd_example& myStruct){
Json::Value val;val["name"] = (myStruct.name);
return val;
};
}
