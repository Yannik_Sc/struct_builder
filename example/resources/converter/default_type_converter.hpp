#ifndef STRUCT_BUILDER_DEFAULT_TYPE_CONVERTER_HPP
#define STRUCT_BUILDER_DEFAULT_TYPE_CONVERTER_HPP

#include <json/value.h>
#include <string>
#include <vector>

namespace struct_converter_functions
{
    int convert_integer(const Json::Value& value);

    float convert_float(const Json::Value& value);

    bool convert_boolean(const Json::Value& value);

    std::string convert_string(const Json::Value& value);

    std::vector<std::string> convert_strings(const Json::Value& value);
}

#endif //STRUCT_BUILDER_DEFAULT_TYPE_CONVERTER_HPP
