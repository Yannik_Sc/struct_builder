#include "default_type_converter.hpp"

namespace struct_converter_functions
{
    int convert_integer(const Json::Value &value)
    {
        return value.asInt();
    }

    float convert_float(const Json::Value &value)
    {
        return value.asFloat();
    }

    bool convert_boolean(const Json::Value &value)
    {
        return value.asBool();
    }

    std::string convert_string(const Json::Value &value)
    {
        return value.asString();
    }

    std::vector<std::string> convert_strings(const Json::Value &value)
    {
        if (!value.isArray()) return {};

        std::vector<std::string> strings;

        for(auto val : value) {
            strings.push_back(val.asString());
        }

        return strings;
    }
}
